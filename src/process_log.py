from datetime import datetime
import json
import logging
import numpy as np
import networkx as nx
import sys


class Process:
    ''' Class to read and process streams'''
    def __init__(self):
        self.graph = nx.Graph()

    def run(self):
        ''' main function to run'''
        self._build_graph(sys.argv[1])
        self._write_flagged(sys.argv[3])

    def _anomalous(self, node, degree, amount):
        ''' detect anomalous purchases '''
        purchases = self._get_last_purchases(node, degree)
        if purchases is not None and len(purchases) > 2:
            return np.mean(purchases), np.std(purchases), amount > np.mean(purchases) + 3 * np.std(purchases)
        else:
            return 0, 0, False

    def _get_last_purchases(self, node, degree):
        ''' get the last purchases in the social network of node `node` of degree `degree` '''
        neighbors = nx.single_source_shortest_path(self.graph, node, cutoff=degree)
        transactions = {}
        time = nx.get_node_attributes(self.graph, 'time')
        for neighbor in neighbors:
            try:
                transactions.update(time[neighbor])
            except KeyError:
                pass
        purchases = []
        if len(transactions) == 1:
            for timekey in transactions:
                if len(transactions[timekey]) == 1:
                    return None
                else:
                    for timekey_index in sorted(transactions[timekey], reverse=True):
                        purchases.append(transactions[timekey][timekey_index])
                        if len(purchases) > self.transactions:
                            return purchases
        else:
            for timekey in sorted(transactions, reverse=True):
                if len(transactions[timekey]) == 1:
                    for timekey_index in transactions[timekey]:
                        purchases.append(transactions[timekey][timekey_index])
                        if len(purchases) > self.transactions:
                            return purchases
                else:
                    for timekey_index in sorted(transactions[timekey], reverse=True):
                        purchases.append(transactions[timekey][timekey_index])
                        if len(purchases) > self.transactions:
                            return purchases
        if purchases < 2:
            return None
        else:
            return purchases

    def _build_graph(self, batch):
        ''' builds graph from initial json '''
        with open(batch) as batch_log:
            line = batch_log.readline()
            decoded = json.JSONDecoder().decode(line)
            self.degree = int(decoded['D'])
            self.transactions = int(decoded['T'])
            current_time = None
            timestamp_index = 0
            for line in batch_log:
                decoded = json.JSONDecoder().decode(line)
                try:
                    event = decoded['event_type']
                except KeyError:
                    logging.exception("badly formed line")
                try:
                    id1 = int(decoded['id1'])
                    id2 = int(decoded['id2'])
                    if event == 'befriend':
                        self.graph.add_edge(id1, id2)
                    elif event == 'unfriend':
                        self.graph.remove_edge(id1, id2)
                    else:
                        logging.exception("badly formed friend event")
                except KeyError:
                    if event == 'purchase':
                        try:
                            node = int(decoded['id'])
                            amount = float(decoded['amount'])
                            timestamp = datetime.strptime(decoded['timestamp'], '%Y-%m-%d %H:%M:%S')
                            if current_time == timestamp:
                                timestamp_index += 1
                            else:
                                current_time = timestamp
                                timestamp_index = 0
                        except KeyError:
                            logging.exception("badly formed purchase")
                        except ValueError:
                            logging.exception("bad number value")
                        try:
                            time = nx.get_node_attributes(self.graph, 'time')
                            time[node][timestamp][timestamp_index] = amount
                        except KeyError:
                            self.graph.add_node(node, time={timestamp: {timestamp_index: amount}})
                    else:
                        logging.error("badly formed event_type")

    def _get_transactions(self, stream):
        ''' get transactions from stream_log '''
        transactions = []
        with open(stream) as stream_file:
            for line in stream_file:
                decoded = json.JSONDecoder().decode(line)
                try:
                    mean, sd, anomalous = self._anomalous(int(decoded['id']), self.degree, float(decoded['amount']))
                    if anomalous:
                        decoded["mean"] = mean
                        decoded["sd"] = sd
                        line = line[:-1] + ', "'
                        line += 'mean": "%.2f", "' % mean
                        line += 'sd": "%.2f"}' % sd
                        transactions.append(line)
                except KeyError:
                    logging.exception("badly formed transaction")
                except ValueError:
                    logging.exception("badly formed integer or float")
        return transactions

    def _write_flagged(self, output):
        ''' flag transactions as anomalous '''
        with open(output, "w") as output_file:
            transactions = self._get_transactions(sys.argv[2])
            for transaction in transactions:
                output_file.write(transaction)


if __name__ == "__main__":
    p = Process()
    p.run()
